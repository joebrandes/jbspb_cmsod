<?php
defined('TYPO3') or die('Access denied.');

call_user_func(function()
{
    /**
     * Temporary variables
     */
    $extensionKey = 'jbspb_cmsod';

    /**
     * Default TypoScript for JbspbBootswatch
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        $extensionKey,
        'Configuration/TypoScript',
        'JBSPB CMSOD'
    );
});
