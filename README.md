Sitepackage for the project "JBSPB Bootstrapmade CMSOD"
==============================================================

Trying to get the SP going for TYPO3 v12 on PVE LAMP with Debian 12 (Bookworm).

If you want the TYPO3 v10 Version look for Tag **v10**.

![TYPO3 und Bootswatch Template](./Resources/Public/Images/typo3-plus-bootstrapmade.png)

Use
---

Sitepackagebuild for CMSOD Project www.cms-online-designer.de and the RST-TYPO3-Project

Link
----

<https://gitlab.com/joebrandes/jbspb_bootswatch>
