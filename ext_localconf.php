<?php
defined('TYPO3') or die('Access denied.');

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['jbspb_cmsod'] = 'EXT:jbspb_cmsod/Configuration/RTE/Default.yaml';

/***************
 * PageTS
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:jbspb_cmsod/Configuration/TsConfig/Page/All.tsconfig">');


/***************
 * Register Icons
 */
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$iconRegistry->registerIcon(
    'systeminformation-jbspb',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:jbspb_cmsod/Resources/Public/Icons/SystemInformation/bootstrappackage.svg']
);
$icons = [
    'accordion',
    'accordion-item',
    'card-group',
    'card-group-item',
    'carousel',
    'carousel-item',
    'carousel-item-backgroundimage',
    'carousel-item-calltoaction',
    'carousel-item-header',
    'carousel-item-html',
    'carousel-item-image',
    'carousel-item-text',
    'carousel-item-textandimage',
    'beside-text-img-centered-left',
    'beside-text-img-centered-right',
    'csv',
    'externalmedia',
    'gallery',
    'icon-group',
    'icon-group-item',
    'listgroup',
    'menu-card',
    'social-links',
    'tab',
    'tab-item',
    'texticon',
    'timeline',
    'timeline-item'
];
foreach ($icons as $icon) {
    $iconRegistry->registerIcon(
        'content-jbspb-bootswatch-' . $icon,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:jbspb_cmsod/Resources/Public/Icons/ContentElements/' . $icon . '.svg']
    );
}
