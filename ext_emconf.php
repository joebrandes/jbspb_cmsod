<?php

/**
 * Extension Manager/Repository config file for ext "jbspb_cmsod".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'JBSPB CMSOD',
    'description' => 'Sitepackagebuild for CMSOD 2021',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '12.4.0-12.4.99',
            'fluid_styled_content' => '12.4.0-12.4.99',
            'rte_ckeditor' => '12.4.0-12.4.99',
            'indexed_search' => '12.4.0-12.4.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Becss\\JbspbCmsod\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Joe Brandes',
    'author_email' => 'joe.brandes@gmail.com',
    'author_company' => 'BECSS',
    'version' => '12.4.99',
];
